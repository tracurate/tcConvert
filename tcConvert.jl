#!/usr/bin/env julia

using Pkg
required_pkgs = [PackageSpec(name = "ArgParse"),
		  PackageSpec(name = "Images"),
		  PackageSpec(name = "LightXML"),
		  PackageSpec(name = "HDF5", version = "0.13"),
		  PackageSpec(name = "ProgressMeter"),
		  PackageSpec(name = "Dates"),
		  PackageSpec(name = "CSV"),
		  PackageSpec(name = "DataFrames", version = "0.21"),
		  PackageSpec(name = "Contour"),
		  PackageSpec(name = "QuartzImageIO"),
		  PackageSpec(name = "ImageMagick"),
		  PackageSpec(name = "JSON"),
		  PackageSpec(name = "ImageCore")]
Pkg.add(required_pkgs)

using ArgParse: @add_arg_table, ArgParseSettings, parse_args
using CSV: File
using Contour: contours, coordinates, lines, levels
using DataFrames: DataFrame!
using Dates: DateTime, now, format
using HDF5: HDF5File, HDF5Group, exists, h5g_open, h5g_create, h5l_create_soft, _link_properties, H5P_DEFAULT, H5F_LIBVER_EARLIEST, H5F_LIBVER_V18, h5read, h5open, h5d_get_space, h5s_get_simple_extent_dims, h5l_get_info, g_create
using Images: RGBX, Normed, load, Gray
using ImageCore: rawview, channelview, permuteddimsview
using LightXML: parse_file, root, name, child_elements, find_element, content, free, XMLElement
using ProgressMeter: @showprogress

import Base.+, Base.-, Base.readdir
import JSON

mutable struct Point{T}
    x::T
    y::T
end

function crop!(x::Point, x_lims, y_lims)
    (x.x < x_lims[1]) && (x.x = x_lims[1])
    (x.x > x_lims[2]) && (x.x = x_lims[2])
    (x.y < y_lims[1]) && (x.y = y_lims[1])
    (x.y > y_lims[2]) && (x.y = y_lims[2])
    return x
end

struct Offset{T}
    x::T
    y::T
end

(+)(x::Point, y::Offset)   = Point( x.x + y.x, x.y + y.y)
(+)(x::Point, y::Point)    = Point( x.x + y.x, x.y + y.y)
(+)(x::Offset, y::Offset)  = Offset(x.x + y.x, x.y + y.y)
(+)(x::Point, y::Integer)  = Point( x.x + y,   x.y + y)
(+)(x::Offset, y::Integer) = Offset(x.x + y,   x.y + y)
(-)(x::Point, y::Offset)   = Point( x.x - y.x, x.y - y.y)
(-)(x::Point, y::Point)    = Point( x.x - y.x, x.y - y.y)
(-)(x::Offset, y::Offset)  = Offset(x.x - y.x, x.y - y.y)
(-)(x::Point, y::Integer)  = Point( x.x - y,   x.y - y)
(-)(x::Offset, y::Integer) = Offset(x.x - y,   x.y - y)

struct Rectangle{T}
    top_left::Point{T}
    bottom_right::Point{T}
end

struct Polygon{T}
    points::Array{Point{T}, 1}
end

Base.string(x::Point) = "$(typeof(x))($(string(x.x)), $(string(x.y)))"
Base.string(x::Offset) = "$(typeof(x))($(string(x.x)), $(string(x.y)))"
Base.string(x::Rectangle) = "$(typeof(x))(($(x.top_left.x),$(x.top_left.y))x($(x.bottom_right.x),$(x.bottom_right.y)))"
Base.string(x::Polygon) = begin
    out = "$(typeof(x))(\n"
    for p in x.points
        out *= "  $(string(p))\n"
    end
    out *= ")"
end
Base.repr(x::Point) = string(x)
Base.repr(x::Offset) = string(x)
Base.repr(x::Rectangle) = string(x)
Base.repr(x::Polygon) = string(x)
Base.show(io::IO, mime::MIME"text/plain", x::Point) = print(string(x))
Base.show(io::IO, mime::MIME"text/plain", x::Offset) = print(string(x))
Base.show(io::IO, mime::MIME"text/plain", x::Rectangle) = print(string(x))
Base.show(io::IO, mime::MIME"text/plain", x::Polygon) = print(string(x))

function readdir(path::AbstractString, dotfiles::Bool)
    files = readdir(path)
    if !dotfiles
        files = filter(x -> !startswith(x, "."), files)
    end
    return files
end

mutable struct Object
    object_id::UInt32
    frame_id::UInt32
    slice_id::UInt32
    channel_id::UInt16

    centroid::Point{UInt16}
    bounding_box::Rectangle{UInt16}
    packed_mask::BitArray
    outline::Polygon{UInt32}
end

struct EventType
    id::UInt16
    name::String
    description::String
end
EventDeath    = EventType(0, "cell_death",    "The cell is identified as dead.")
EventDivision = EventType(1, "cell_division", "The cell divides in 2 or more other cells.")
EventLost     = EventType(2, "cell_lost",     "The cell is lost during the tracking process.")
EventMerge    = EventType(3, "cell_merge",    "Two or more cells are merged to on object.")
EventUnmerge  = EventType(4, "cell_unmerge",  "Two or more cells are merged to on object.")
EventEOM      = EventType(5, "end_of_movie",  "The cell track ends at the end of the movie.")

all_events = [EventDeath, EventDivision, EventLost, EventMerge, EventUnmerge, EventEOM]

mutable struct AutoTracklet
    tID::UInt32
    objects::Dict{UInt32, UInt32}
    next::Array{UInt32, 1}
    next_event::Union{EventType, Nothing}
    prev::Array{UInt32, 1}
    prev_event::Union{EventType, Nothing}
    start_tp::UInt32
    end_tp::UInt32
end

mutable struct Tracklet
    tID::UInt32
    objects::Dict{UInt32, UInt32}
    next::Array{UInt32, 1}
    next_event::Union{EventType, Nothing}
    prev::Array{UInt32, 1}
    prev_event::Union{EventType, Nothing}
    start_tp::UInt32
    end_tp::UInt32
end

mutable struct Info
    coordinate_format::String
    data_format_version::String
    inputFiles::Array{String, 1}
    timeOfConversion::DateTime
end

struct InternalRepresentation
    objects::Array{Object,1}
    autoTracklets::Array{AutoTracklet, 1}
    tracklets::Array{Tracklet, 1}
    images::Array{Array{RGBX{Normed{UInt8, 8}}, 2}, 1}
    info::Info
end

abstract type DataFormat end

struct CellTrackerDataFormat <: DataFormat
    tracks::String
    xml::String
    images::String
end

struct TraCurateDataFormat <: DataFormat
    file::String
end

struct CellProfilerDataFormat <: DataFormat
    file::String
end

struct ILastikDataFormat <: DataFormat
    file::String
    csv::Union{String, Nothing}
    path::String
    images::Union{String, Nothing}
end

struct BioTracksDataFormat <: DataFormat
    meta::String
end

struct CSVDataFormat <: DataFormat
    file::String
    images::Union{String, Nothing}
    id_col::Union{Symbol, Nothing}
    frame_col::Symbol
    slice_col::Union{Symbol, Nothing}
    chan_col::Union{Symbol, Nothing}
    coord_col::Tuple{Symbol, Symbol}
    bb_col::Union{Nothing, Tuple{Symbol, Symbol, Symbol, Symbol}}
    track_col::Union{Symbol, Nothing}
    quirks::Dict{String, Bool}
end

function importData(input::DataFormat)::InternalRepresentation
    @info "Importing from " * string(typeof(input))
    @info "  Reading metadata"
    info = readInfo(input)
    @info "  Reading objects"
    objects = readObjects(input)
    @info "  Reading autotracklets"
    autotracklets = readAutoTracklets(input)
    @info "  Reading images"
    images = readImages(input)

    return InternalRepresentation(objects, autotracklets, Array{Tracklet, 1}(), images, info)
end

function exportData(internal::InternalRepresentation, output::DataFormat;
        writeInfo = writeInfo,
        writeEvents = writeEvents,
        writeImages = writeImages,
        writeObjects = writeObjects,
        writeAutoTracklets = writeAutoTracklets,
        writeTracklets = writeTracklets,
        writeAnnotations = writeAnnotations)
    @info "Exporting to " * string(typeof(output))
    @info "  Writing info"
    writeInfo(internal, output) || return false
    @info "  Writing events"
    writeEvents(internal, output) || return false
    @info "  Writing images"
    writeImages(internal, output) || return false
    @info "  Writing objects"
    writeObjects(internal, output) || return false
    @info "  Writing autotracklets"
    writeAutoTracklets(internal, output) || return false
    @info "  Writing tracklets"
    writeTracklets(internal, output) || return false
    @info "  Writing annotations"
    writeAnnotations(internal, output) || return false
    return true
end

function Base.convert(input::DataFormat, output::DataFormat)
    @info "Converting from " * string(typeof(input)) * " to " * string(typeof(output))
    internal = importData(input)
    rm(output.file, force = true)
    return exportData(internal, output)
end

function readInfo(input::CellTrackerDataFormat)
    cf = "Cartesian"
    df = "1.0"
    im = joinpath(input.images, sort(readdir(input.images, false))[1])
    ifs = Array{String, 1}([input.xml, input.tracks, im])
    toc = now()
    return Info(cf, df, ifs, toc)
end

# Read objects
function readObjects(file::TraCurateDataFormat)
    objects = Array{Object, 1}()

    h5open(file.file, "r", swmr=true) do f
        for frame in f["objects/frames"]
            for slice in frame["slices"]
                for channel in slice["channels"]
                    for object in channel["objects"]
                        r = read(object)
                        oID = r["object_id"]
                        fID = r["frame_id"]
                        sID = r["slice_id"]
                        cID = r["channel_id"]

                        ctr = Point{UInt16}(r["centroid"]...)

                        bb_5 = r["bounding_box"]
                        bb = Rectangle{UInt16}(Point{UInt16}(bb_5[1,:]...), Point{UInt16}(bb_5[2,:]...))

                        # TODO: bitmask

                        ol_5 = r["outline"]
                        ol_ar = reshape(mapslices(x -> Point{UInt32}(x...), ol_5, dims = 1), (axes(ol_5, 2).stop,))
                        ol = Polygon{UInt32}(ol_ar)

                        o = Object(oID, fID, sID, cID, ctr, bb, BitArray{1}(), ol)
                        push!(objects, o)
                    end
                end
            end
        end
    end
    return objects
end


function readObjects(ct_ds::CellTrackerDataFormat)
    objects = Array{Object, 1}()
    y_size = size(load(joinpath.(ct_ds.images, sort(readdir(ct_ds.images, false)))[1]))[2]

    xml_files = joinpath.(ct_ds.xml, sort(readdir(ct_ds.xml, false)))
    @showprogress for xml_file in xml_files
        xml_doc = parse_file(xml_file)
        xml_root = root(xml_doc)

        fID::UInt32 = parse(UInt32, split(name(xml_root), "_")[2]) - 1
        sID::UInt32 = 0
        cID::UInt16 = 0

        for xml_obj in child_elements(xml_root)
            oID::UInt32 = parse(UInt32, content(find_element(xml_obj["ObjectID"][1], "value")))

            xml_ctr = xml_obj["ObjectCenter"][1]["point"][1]
            ctr_x = floor(UInt16, parse(Float64, content(find_element(xml_ctr, "x"))))
            ctr_y = floor(UInt16, y_size - parse(Float64, content(find_element(xml_ctr, "y"))))
            ctr = Point{UInt16}(ctr_x, ctr_y)

            xml_bb = xml_obj["ObjectBoundingBox"][1]["point"]
            bb_p1_x = parse(UInt16, content(find_element(xml_bb[1], "x")))
            bb_p1_y = UInt16(y_size - parse(UInt16, content(find_element(xml_bb[1], "y"))))
            bb_p2_x = parse(UInt16, content(find_element(xml_bb[2], "x")))
            bb_p2_y = UInt16(y_size - parse(UInt16, content(find_element(xml_bb[2], "y"))))
            bb = Rectangle{UInt16}(Point{UInt16}(bb_p1_x, bb_p1_y), Point{UInt16}(bb_p2_x, bb_p2_y))

            xml_ol = xml_obj["Outline"][1]["point"]
            ol = Array{Point{UInt32}, 1}()
            ol_x = round.(UInt32, parse.(Float64, content.([xml_pt["x"][1] for xml_pt in xml_ol])))
            ol_y = round.(UInt32, parse.(Float64, content.([xml_pt["y"][1] for xml_pt in xml_ol])))
            ol = Polygon{UInt32}([Point{UInt32}(x, y_size - y) for (x, y) in zip(ol_x, ol_y)])

            o = Object(oID, fID, sID, cID, ctr, bb, BitArray{1}(), ol)
            push!(objects, o)
            # TODO: trackID?
        end

        free(xml_doc)
    end

    return objects
end

readAutoTracklets(ct_ds::CellTrackerDataFormat) = readAutoTracklets(ct_ds, true)

function readAutoTracklets(ct_ds::CellTrackerDataFormat, omitSingleCellTracks::Bool)
    autotracklets = Array{AutoTracklet, 1}()

    xml_file = ct_ds.tracks
    xml_doc = parse_file(xml_file)
    xml_root = root(xml_doc)

    tIDctr::UInt32 = 0
    tID::UInt32 = 0

    xml_tracks = Array{XMLElement, 1}([x for x in child_elements(xml_root)])
    @showprogress for xml_track in xml_tracks
        objects = Dict([
            (
                UInt32(parse(UInt32, content(o["Time"][1])) - 1),
                parse(UInt32, content(o["ObjectID"][1]))
            ) for o in xml_track["object"]])

        if omitSingleCellTracks
            if length(objects) < 2
                continue
            else
                tID = tIDctr
                tIDctr += 1
            end
        else
            tID = UInt32(parse(UInt32, content(xml_track["TrackID"][1])) - 1)
        end

        start_tp = min(keys(objects)...)
        end_tp = max(keys(objects)...)

        next = Array{UInt32, 1}()
        next_event = nothing
        prev = Array{UInt32, 1}()
        prev_event = nothing

        autotracklet = AutoTracklet(tID, objects, next, next_event, prev, prev_event, start_tp, end_tp)

        push!(autotracklets, autotracklet)
    end

    return autotracklets
end

function readImages(ct_ds::CellTrackerDataFormat)
    image_files = joinpath.(ct_ds.images, sort(readdir(ct_ds.images, false)))

    images = Array{Array{RGBX{Normed{UInt8,8}},2},1}()
    @showprogress for image_file in image_files
        push!(images, load(image_file))
    end

    return images
end

function openOrCreateGroup(parent::Union{HDF5File, HDF5Group}, path::String)
    return (exists(parent, path) ? g_open : g_create)(parent, path)
end

function writeObject(object::Object, o_grp::HDF5Group)
    o_grp["object_id"] = object.object_id
    o_grp["frame_id"] = object.frame_id
    o_grp["slice_id"] = object.slice_id
    o_grp["channel_id"] = object.channel_id

    ctr = object.centroid
    o_grp["centroid"] = Array{UInt16, 2}(reshape([ctr.x, ctr.y], (2, 1)))
    bb = object.bounding_box
    x1, y1 = bb.top_left.x, bb.bottom_right.y
    x2, y2 = bb.bottom_right.x, bb.top_left.y
    o_grp["bounding_box"] = Array{UInt16, 2}(reshape([x1, y1,
                                                      x2, y2], (2, 2)))

    o_grp["outline"] = hcat([[p.x, p.y] for p in object.outline.points]...)
    return true
end

createSoftLink(grp::HDF5Group, target::String, name::String) =
    h5l_create_soft(target, grp.id, name, _link_properties(name), H5P_DEFAULT)

function writeObjects(internal::InternalRepresentation, output::TraCurateDataFormat)
    nframes = UInt32(length(internal.images))
    nslices = UInt16(1)
    slice_shape = Array{UInt16, 1}([1, 1])
    im_size = size(internal.images[1])
    dimensions = Array{UInt32, 1}([im_size[1], im_size[2]])
    nchannels = UInt16(1)

    h5open(output.file, "cw", "libver_bounds", (H5F_LIBVER_EARLIEST, H5F_LIBVER_V18)) do f
        # Create the structure
        objs_grp = openOrCreateGroup(f, "objects")
        @showprogress for o in internal.objects
            a_grp_name = "/objects"
            fs_grp_name = a_grp_name * "/frames/"
            f_grp_name = fs_grp_name * string(o.frame_id)
            ss_grp_name = f_grp_name * "/slices/"
            s_grp_name = ss_grp_name * string(o.slice_id)
            cs_grp_name = s_grp_name * "/channels/"
            c_grp_name = cs_grp_name * string(o.channel_id)
            os_grp_name = c_grp_name * "/objects/"
            o_grp_name = os_grp_name * string(o.object_id)

            if ! exists(f, a_grp_name)
                a_grp = g_create(f, a_grp_name)
                createSoftLink(a_grp, "/images/frame_rate", "frame_rate")
                createSoftLink(a_grp, "/images/nframes", "nframes")
                createSoftLink(a_grp, "/images/nslices", "nslices")
                createSoftLink(a_grp, "/images/slice_shape", "slice_shape")
            end
            if ! exists(f, fs_grp_name)
                fs_grp = g_create(f, fs_grp_name)
            end
            if ! exists(f, f_grp_name)
                f_grp = g_create(f, f_grp_name)
                f_grp["frame_id"] = o.frame_id
            end
            if ! exists(f, ss_grp_name)
                ss_grp = g_create(f, ss_grp_name)
            end
            if ! exists(f, s_grp_name)
                s_grp = g_create(f, s_grp_name)
                createSoftLink(s_grp, "/images/frames/$(o.frame_id)/slices/$(o.slice_id)/dimensions", "dimensions")
                createSoftLink(s_grp, "/images/frames/$(o.frame_id)/slices/$(o.slice_id)/nchannels", "nchannels")
                s_grp["slice_id"]   = UInt16(o.slice_id)
            end
            if ! exists(f, cs_grp_name)
                cs_grp = g_create(f, cs_grp_name)
            end
            if ! exists(f, c_grp_name)
                c_grp = g_create(f, c_grp_name)
                c_grp["channel_id"] = UInt16(o.channel_id)
            end
            if ! exists(f, os_grp_name)
                os_grp = g_create(f, os_grp_name)
            end
            if ! exists(f, o_grp_name)
                o_grp = openOrCreateGroup(f, o_grp_name)
                if ! writeObject(o, o_grp)
                    return false
                end
            end
        end
    end
    return true
end

function writeEvents(internal::InternalRepresentation, output::TraCurateDataFormat)
    h5open(output.file, "cw", "libver_bounds", (H5F_LIBVER_EARLIEST, H5F_LIBVER_V18)) do f
        ev_grp = openOrCreateGroup(f, "events")

        @showprogress for event in all_events
            e_grp = openOrCreateGroup(ev_grp, event.name)
            e_grp["event_id"]    = event.id
            e_grp["name"]        = event.name
            e_grp["description"] = event.description
        end
    end
    return true
end

function writeAutoTracklets(internal::InternalRepresentation, output::TraCurateDataFormat)
    h5open(output.file, "cw", "libver_bounds", (H5F_LIBVER_EARLIEST, H5F_LIBVER_V18)) do f
        ats_grp = openOrCreateGroup(f, "autotracklets")
        @showprogress for autotracklet in internal.autoTracklets
            tID = autotracklet.tID
            objs = autotracklet.objects

            path = join(string.(["autotracklets", tID]), "/")
            at_grp = openOrCreateGroup(f, path)

            start_tp = UInt32(min(keys(objs)...))
            end_tp = UInt32(max(keys(objs)...))

            at_grp["autotracklet_id"] = tID
            at_grp["start"] = start_tp
            at_grp["end"] = end_tp

            obj_grp = openOrCreateGroup(at_grp, "objects")
            os = autotracklet.objects
            for (idx, f) in enumerate(sort(UInt32.(keys(os))))
                createSoftLink(obj_grp, "/objects/frames/$(f)/slices/0/channels/0/objects/$(os[f])", string(idx - 1))
            end

            # TODO: next, next_event, prev, prev_event
            if (autotracklet.next_event != nothing)
                @error "Unimplemented"
                at_grp["next"]
                at_grp["next_event"]

            end
            if (autotracklet.prev_event != nothing)
                @error "Unimplemented"
                at_grp["prev"]
                at_grp["prev_event"]
            end
        end
    end
    return true
end

function writeImages(internal::InternalRepresentation, output::TraCurateDataFormat)
    h5open(output.file, "cw", "libver_bounds", (H5F_LIBVER_EARLIEST, H5F_LIBVER_V18)) do f
        ims_grp = openOrCreateGroup(f, "images")
        @showprogress for (idx, image) in enumerate(internal.images)
            i = idx - 1
            is_name = "images"
            fs_name = is_name * "/" * "frames"
            f_name  = fs_name * "/" * string(i)
            ss_name =  f_name * "/" * "slices"
            s_name  = ss_name * "/" * "0"
            cs_name =  s_name * "/" * "channels"

            if ! exists(f, is_name)
                im_grp = openOrCreateGroup(f, is_name)
                im_grp["nframes"] = UInt32(length(internal.images))
                im_grp["nslices"] = UInt16(1)
                im_grp["frame_rate"] = Float32(-1)
                im_grp["slice_shape"] = Array{UInt16, 1}([1, 1])
                fr_grp = openOrCreateGroup(im_grp, "frames")
            end
            if ! exists(f, fs_name)
                fs_grp = openOrCreateGroup(f, fs_name)
            end
            if ! exists(f, f_name)
                f_grp = openOrCreateGroup(f, f_name)
                f_grp["frame_id"] = UInt32(i)
            end
            if ! exists(f, ss_name)
                ss_grp = openOrCreateGroup(f, ss_name)
                s = size(internal.images[1])
            end
            if ! exists(f, s_name)
                s_grp = openOrCreateGroup(f, s_name)
                s_grp["dimensions"] = Array{UInt32, 1}([s[1], s[2]])
                s_grp["nchannels"] = UInt16(1)
                s_grp["slice_id"] = UInt16(0)
            end
            if ! exists(f, cs_name)
                cs_grp = openOrCreateGroup(f, cs_name)
                # TODO: find better way?
                # cs_grp["0"] = rotr90(convert(Array{UInt8}, round.(convert(Array{Float32}, Gray.(image)) * 255)))[:,end:-1:1]
                #cs_grp["0", "compress", 5] = permutedims(convert(Array{UInt8}, round.(convert(Array{Float32}, Gray.(image)) * 255)), (2, 1))
                cv = channelview(image)
                rv = rawview(cv)
                rva = Array{UInt8, 3}(rv)
                pdv = permutedims(rva, (1, 3, 2))
                #pdv = rva
                cs_grp["0", "compress", 5] = pdv
                #cs_grp["0", "compress", 5] = rawview(permuteddimsview(image, (2, 1)))
            end
        end
    end
    return true
end

function writeInfo(internal::InternalRepresentation, output::TraCurateDataFormat)
    h5open(output.file, "cw", "libver_bounds", (H5F_LIBVER_EARLIEST, H5F_LIBVER_V18)) do f
        f["coordinate_format"] = internal.info.coordinate_format
        f["data_format_version"] = internal.info.data_format_version
        inf_grp = openOrCreateGroup(f, "info")
        inf_grp["inputFiles"] = internal.info.inputFiles
        inf_grp["timeOfConversion"] = format(internal.info.timeOfConversion, "dd-mm-yyyy-HH:MM:SS")
    end
    return true
end

function writeTracklets(internal::InternalRepresentation, output::TraCurateDataFormat)
    return true
end

function writeAnnotations(internal::InternalRepresentation, output::TraCurateDataFormat)
    h5open(output.file, "cw", "libver_bounds", (H5F_LIBVER_EARLIEST, H5F_LIBVER_V18)) do f
        an_grp = openOrCreateGroup(f, "annotations")
        oa_grp = openOrCreateGroup(an_grp, "object_annotations")
        ta_grp = openOrCreateGroup(an_grp, "track_annotations")
    end
    return true
end

function readInfo(input::CellProfilerDataFormat)
    cf = "Cartesian"
    df = "1.0"
    ifs = Array{String, 1}([input.file])
    toc = now()
    return Info(cf, df, ifs, toc)
end

function readObjects(input::CellProfilerDataFormat)
    global OBJ_CTR
    OBJ_CTR = UInt32(0)
    objects = Array{Object, 1}()

    dat = File(input.file) |> DataFrame!

    y_size, x_size = size(load(split(dat[1,:Metadata_FileLocation], ":")[2]))

    for frame in unique(dat.Metadata_FrameNumber)
        for object in dat[dat.Metadata_FrameNumber .== frame, :].ObjectNumber
            o_dat = dat[(dat.Metadata_FrameNumber .== frame) .&
                        (dat.ObjectNumber .== object), :]
            oID = OBJ_CTR
            OBJ_CTR += UInt32(1)
            fID = frame
            sID = 0
            cID = 0
            ctr = Point{UInt16}(round(UInt16, o_dat.Location_Center_X[1]),
                                round(UInt16, y_size - o_dat.Location_Center_Y[1]))

            pad = round(UInt16, 0.25 * sqrt(o_dat.AreaShape_Area[1]))

            bb1 = crop!(ctr - pad, (0x0000, x_size), (0x0000, y_size))
            bb2 = crop!(ctr + pad, (0x0000, x_size), (0x0000, y_size))

            bb = Rectangle{UInt16}(bb1, bb2)
            pm = BitArray{1}()

            ol = Polygon{UInt32}([
                crop!(Point{UInt32}(ctr.x + pad, ctr.y + pad), (0x0000, x_size), (0x0000, y_size)),
                crop!(Point{UInt32}(ctr.x - pad, ctr.y + pad), (0x0000, x_size), (0x0000, y_size)),
                crop!(Point{UInt32}(ctr.x - pad, ctr.y - pad), (0x0000, x_size), (0x0000, y_size)),
                crop!(Point{UInt32}(ctr.x + pad, ctr.y - pad), (0x0000, x_size), (0x0000, y_size)),
                crop!(Point{UInt32}(ctr.x + pad, ctr.y + pad), (0x0000, x_size), (0x0000, y_size))
            ])

            o = Object(oID, fID, sID, cID, ctr, bb, pm, ol)
            push!(objects, o)
        end
    end
    return objects
end

function readAutoTracklets(input::CellProfilerDataFormat)
    autotracklets = Array{AutoTracklet, 1}()

    global OBJ_CTR, AT_CTR
    OBJ_CTR = UInt32(0)

    dat = File(input.file) |> DataFrame!

    id_map = Dict{Tuple{UInt32, UInt32}, UInt32}()
    for frame in unique(dat.Metadata_FrameNumber)
        for object in dat[dat.Metadata_FrameNumber .== frame, :].ObjectNumber
            o_dat = dat[(dat.Metadata_FrameNumber .== frame) .&
                        (dat.ObjectNumber .== object), :]
            oID = OBJ_CTR
            OBJ_CTR += UInt32(1)

            fID = convert(UInt32, o_dat.Metadata_FrameNumber[1])
            atID = convert(UInt32, o_dat.ObjectNumber[1])

            id_map[(fID, atID)] = oID
        end
    end

    for at in unique(dat.ObjectNumber)
        tID = convert(UInt32, at)
        objs = Dict{UInt32, UInt32}()
        for (frame, atID) in keys(id_map)
            if atID == at
                objs[frame] = id_map[(frame, atID)]
            end
        end

        next = Array{UInt32, 1}()
        next_event = nothing
        prev = Array{UInt32, 1}()
        prev_event = nothing

        tp_start = UInt32(min(dat.Metadata_FrameNumber...))
        tp_end   = UInt32(max(dat.Metadata_FrameNumber...))
        at = AutoTracklet(tID, objs, next, next_event, prev, prev_event, tp_start, tp_end)
        push!(autotracklets, at)
    end

    return autotracklets
end

function readImages(input::CellProfilerDataFormat)
    dat = File(input.file) |> DataFrame!
    images = Array{Array{RGBX{Normed{UInt8, 8}}, 2}, 1}()
    for frame in unique(dat[:, :Metadata_FrameNumber])
        f_dat = unique(dat[dat.Metadata_FrameNumber .== frame, :Metadata_FileLocation])
        if (length(f_dat) != 1)
            @error "Multiple image paths for frame $(frame): $(f_dat)"
        end
        f_dat = split(f_dat[1], ":")[2]
        im = load(f_dat)
        push!(images, im)
    end
    return images
end

function bitmask2polygon(im::BitArray{2})
    # im = convert(Array{UInt32, 2}, im)
    w, h = size(im)
    x = 1:1.0:w
    y = 1:1.0:h
    z = convert(Array{Float64, 2}, im)

    c = contours(x, y, z, 1)
    xs, ys = coordinates(lines(levels(c)[1])[1])

    ol = [Point{UInt32}(round(UInt32, x), round(UInt32, h - y)) for (x, y) in zip(xs, ys)]

    return ol
end

function extractPolygons(img::Array{UInt32, 2})
    ids = sort(unique(img))
    ids = ids[ids .!= 0] # remove background
    out = fill(Array{Point{UInt32},1}(), size(ids))
    Threads.@threads for idIdx in 1:size(ids)[1]
        tID = ids[idIdx]
        out[idIdx] = bitmask2polygon(img .== tID)
    end
    dict = Dict{UInt32, Array{Point{UInt32}, 1}}(zip(ids, out))
    return dict
end

function extractTracks(filename::String, path::String, fIDs::UnitRange{UInt32})
    dict = Dict{UInt32, Dict{UInt32, Array{Point{UInt32}, 1}}}()
    @showprogress for fID in fIDs
        slice = (Int64(1), :, :, Int64(fID))
        a = h5read(filename, path, slice)
        a = a[1, :, :, 1]
        dict[fID - 1] = extractPolygons(a)
    end
    return dict
end

function extractAllTracks(filename::String, path::String)
    h5open(filename) do h5f
        h5d = h5f[path]
        h5s = h5d_get_space(h5d.id)
        ext = h5s_get_simple_extent_dims(h5s)[1]
        range = UnitRange{UInt32}(1:ext[4])
        ret = extractTracks(filename, path, range)
        return ret
    end
end

function extractImages(filename::String, path::String, range::UnitRange{UInt32})
    ret = Array{Array{RGBX{Normed{UInt8, 8}}, 2}, 1}()
    @showprogress for fID in range
        slice = (Int64(1), :, :, Int64(fID))
        a = h5read(filename, path, slice)
        a = a[1, :, :, 1]
        a = convert.(UInt8, a)
        a = permutedims(a, (2, 1))
        a = convert(Array{RGBX{Normed{UInt8, 8}}, 2}, Gray.(a ./ 255))
        push!(ret, a)
    end
    return ret
end

function readImages(input::ILastikDataFormat)
    h5open(input.file) do h5f
        h5d = h5f[input.path]
        h5s = h5d_get_space(h5d.id)
        ext = h5s_get_simple_extent_dims(h5s)[1]
        range = UnitRange{UInt32}(1:ext[4])
        ret = extractImages(input.file, input.path, range)
        return ret
    end
end

function importData(input::ILastikDataFormat; all::Dict{UInt32,Dict{UInt32,Array{Point{UInt32},1}}}=Dict{UInt32,Dict{UInt32,Array{Point{UInt32},1}}}())::InternalRepresentation
    @info "Importing from " * string(typeof(input))
    @info "  Reading images"

    objects = Array{Object,1}()
    autoTracklets = Array{AutoTracklet, 1}()
    tracklets = Array{Tracklet, 1}()
    images = Array{Array{RGBX{Normed{UInt8, 8}}, 2}, 1}()
    info = Info("Cartesian", "1.0", [input.file, input.path], now())

    if isnothing(input.images)
        # Use segmentation images from HDF5 file
        images = readImages(input)
    else
        # Use images from the directory
        if (isdir(input.images))
            image_files = joinpath.(input.images, sort(readdir(input.images, false)))

            @showprogress for image_file in image_files
                push!(images, load(image_file))
            end
        else
            @error "Image directory is not a directory"
        end
    end

    @info "  Reading objects and autotracklets"
    if isempty(all)
        all = extractAllTracks(input.file, input.path)
    end

    OBJ_ID = UInt32(0)
    sID = UInt32(0)
    cID = UInt16(0)

    if !isnothing(input.csv)
        csv = File(input.csv) |> DataFrame!
    end


    @info "  Restructuring to fit new format"
    @showprogress for (fID, fc) in all
        for (tID, oc) in fc
            ol = Polygon(oc)
            e_x = extrema([pp.x for pp in ol.points])
            e_y = extrema([pp.y for pp in ol.points])
            ctr_x = round(UInt16, (e_x[1] + e_x[2])/2)
            ctr_y = round(UInt16, (e_y[1] + e_y[2])/2)
            ctr = Point{UInt16}(ctr_x, ctr_y)
            bb = Rectangle{UInt16}(Point{UInt16}(e_x[1], e_y[1]), Point{UInt16}(e_x[2], e_y[2]))
            pm = BitArray{1}()
            obj = Object(OBJ_ID, fID, sID, cID, ctr, bb, pm, ol)
            OBJ_ID += 1

            # Find real tID
            if !isnothing(input.csv)
                t_csv = csv[csv.frame .== fID, :]
                t_csv = t_csv[t_csv.labelimageId .== tID, :]
                if (length(t_csv.trackId) == 1)
                    if (t_csv.trackId[1] >= 0)
                        tID = t_csv.trackId[1]
                    else
                        tID = -1
                    end
                else
                    tID = -1
                end
            end

            # Create track if it doesn't exist
            if tID >= 0
                if !any([at.tID == tID for at in autoTracklets])
                    os = Dict{UInt32, UInt32}()
                    next = Array{UInt32, 1}()
                    next_event = nothing
                    prev = Array{UInt32, 1}()
                    prev_event = nothing
                    start_tp = typemax(UInt32)
                    end_tp = typemin(UInt32)
                    tID = round(UInt32, tID)

                    t = AutoTracklet(tID, os, next, next_event, prev, prev_event, start_tp, end_tp)
                    push!(autoTracklets, t)
                end

                t = [ x for x in autoTracklets if x.tID == tID][1]
                t.objects[fID] = obj.object_id

                # Fix start and end
                (fID < t.start_tp) && (t.start_tp = fID)
                (fID > t.end_tp)   && (t.end_tp = fID)
            end

            push!(objects, obj)
        end
    end


    # TODO: read images

    return InternalRepresentation(objects, autoTracklets, Array{Tracklet, 1}(), images, info)
end

function readInfo(input::CSVDataFormat)
    return Info("Cartesian", "1.0", [input.file], now())
end

function readInfo(input::BioTracksDataFormat)
    files = Array{String, 1}()
    open(input.meta, "r") do f
        j = JSON.parse(read(f, String))
        base = dirname(input.meta)
        push!(files, input.meta)

        extra_files = [(x["name"], joinpath(base, x["path"])) for x in j["resources"]]
        for (name, file) in extra_files
            name in ["cmso_objects_table", "cmso_links_table"] &&
                isfile(file) &&
                push!(files, file)
        end
    end
    return Info("Cartesian", "1.0", files, now())
end

function genericBoundingBox(p::Point{UInt16}, size::UInt16, x_size::UInt16, y_size::UInt16)
    bb1 = crop!(Point{Int32}(p.x, p.y) - Int32(div(size, 2)), (0x0000, x_size), (0x0000, y_size))
    bb2 = crop!(Point{Int32}(p.x, p.y) + Int32(div(size, 2)), (0x0000, x_size), (0x0000, y_size))
    return Rectangle{UInt16}(Point{UInt16}(bb1.x, bb1.y), Point{UInt16}(bb2.x, bb2.y))
end

function Base.convert(T::Type{Point{U}}, point::Point) where U
    return Point{U}(convert(U, point.x), convert(U, point.y))
end

function Base.convert(T::Type{Polygon{U}}, poly::Polygon) where U
    return Polygon{U}([convert(Point{U}, p) for p in poly.points])
end

function genericOutline(bb::Rectangle{UInt16}; x_size::UInt16 = Inf16, y_size::UInt16 = Inf16)
    ol =  Polygon{Int32}([
        crop!(Point{Int32}(bb.top_left.x,     bb.top_left.y    ), (0x0000, x_size), (0x0000, y_size)),
        crop!(Point{Int32}(bb.bottom_right.x, bb.top_left.y    ), (0x0000, x_size), (0x0000, y_size)),
        crop!(Point{Int32}(bb.bottom_right.x, bb.bottom_right.y), (0x0000, x_size), (0x0000, y_size)),
        crop!(Point{Int32}(bb.top_left.x,     bb.bottom_right.y), (0x0000, x_size), (0x0000, y_size)),
        crop!(Point{Int32}(bb.top_left.x,     bb.top_left.y    ), (0x0000, x_size), (0x0000, y_size))
    ])
    return convert(Polygon{UInt32}, ol)
end

function genericOutline(p::Point{UInt16}, size::UInt16, x_size::UInt16, y_size::UInt16)
    return genericOutline(genericBoundingBox(p, size, x_size, y_size), x_size = x_size, y_size = y_size)
end

function readObjects(input::CSVDataFormat)
    global OBJ_CTR
    if isnothing(input.id_col) OBJ_CTR = 0 end
    objects = Array{Object, 1}()

    dat = File(input.file) |> DataFrame!

    pad = UInt16(10)
    if isnothing(input.images)
        x_size = round(UInt16, max(dat[:, input.coord_col[1]]...) + pad, RoundUp)
        y_size = round(UInt16, max(dat[:, input.coord_col[2]]...) + pad, RoundUp)
    else
        y_size, x_size = round.(UInt16, size(load(joinpath(input.images, readdir(input.images, false)[1]))))
    end

    @showprogress for row in eachrow(dat)
        global OBJ_CTR

        oID = if !isnothing(input.id_col) row[input.id_col] else OBJ_CTR; OBJ_CTR += 1 end
        fID = row[input.frame_col]
        sID = if !isnothing(input.slice_col) row[input.slice_col] else 0 end
        cID = if !isnothing(input.chan_col) row[input.chan_col] else 0 end
        ctr_x = round(UInt16, row[input.coord_col[1]])
        ctr_y = round(UInt16, row[input.coord_col[2]])
        if input.quirks["flip-y"] ctr_y = y_size - ctr_y end
        ctr = Point{UInt16}(ctr_x, ctr_y)
        if isnothing(input.bb_col)
            bb = genericBoundingBox(ctr, UInt16(pad * 2), x_size, y_size)
        else
            bb = Rectangle{UInt16}(
                Point{UInt16}(round(UInt16, row[input.bb_col[1]]), round(UInt16, row[input.bb_col[2]])),
                Point{UInt16}(round(UInt16, row[input.bb_col[3]]), round(UInt16, row[input.bb_col[4]])))
        end

        ol = genericOutline(bb, x_size = x_size, y_size = y_size)
        pm = BitArray{1}()

        o = Object(oID, fID, sID, cID, ctr, bb, pm, ol)
        push!(objects, o)
    end
    return objects
end

function readObjects(input::BioTracksDataFormat)
    objects = Array{Object, 1}()
    open(input.meta, "r") do f
        j = JSON.parse(read(f, String))
        base = dirname(input.meta)
        file = [x["path"] for x in j["resources"] if x["name"] == "cmso_objects_table"]
        (length(file) == 1 || @error "Either no or multiple resources called cmso_objects_table") &&
            (file = joinpath(base, file[1]))

        dat = File(file) |> DataFrame!

        pad = UInt16(10)
        x_size = round(UInt16, max(dat.cmso_x_coord...) + pad, RoundUp)
        y_size = round(UInt16, max(dat.cmso_y_coord...) + pad, RoundUp)

        @showprogress for row in eachrow(dat)
            oID = row.cmso_object_id
            fID = row.cmso_frame_id
            sID = UInt32(0)
            cID = UInt16(0)

            ctr = Point{UInt16}(round(UInt16, row.cmso_x_coord), round(UInt16, row.cmso_y_coord))
            bb = genericBoundingBox(ctr, UInt16(pad * 2), x_size, y_size)
            ol = genericOutline(bb, x_size = x_size, y_size = y_size)
            pm = BitArray{1}()

            o = Object(oID, fID, sID, cID, ctr, bb, pm, ol)
            push!(objects, o)
        end
    end
    return objects
end

function readAutoTracklets(input::CSVDataFormat)
    autotracklets = Array{AutoTracklet, 1}()

    if isnothing(input.track_col)
        return autotracklets
    end

    dat = File(input.file) |> DataFrame!
    dat[:, :tc_convert_objectID] = 1:Base.size(dat, 1)

    @showprogress for tID in filter(x -> x >= 0, unique(dat[!, input.track_col]))
        tID = UInt32(tID)
        tt = dat[dat[!, input.track_col] .== tID, [input.frame_col, :tc_convert_objectID]]
        objects = Dict{UInt32, UInt32}(zip(tt[!, input.frame_col], tt[!, :tc_convert_objectID]))

        next = Array{UInt32, 1}()
        next_event = nothing
        prev = Array{UInt32, 1}()
        prev_event = nothing

        start_tp = min(keys(objects)...)
        end_tp = max(keys(objects)...)

        at = AutoTracklet(tID, objects, next, next_event, prev, prev_event, start_tp, end_tp)
        push!(autotracklets, at)
    end

    return autotracklets
end

function readAutoTracklets(input::BioTracksDataFormat)
    autotracklets = Array{AutoTracklet, 1}()
    open(input.meta, "r") do f
        j = JSON.parse(read(f, String))
        base = dirname(input.meta)
        file = [x["path"] for x in j["resources"] if x["name"] == "cmso_links_table"]
        (length(file) == 1 || @error "Either no or multiple resources called cmso_links_table") &&
            (file = joinpath(base, file[1]))

        dat_links = File(file) |> DataFrame!

        file = [x["path"] for x in j["resources"] if x["name"] == "cmso_objects_table"]
        (length(file) == 1 || @error "Either no or multiple resources called cmso_objects_table") &&
            (file = joinpath(base, file[1]))

        dat_objects = File(file) |> DataFrame!

        @showprogress for tID in unique(dat_links.cmso_link_id)
            os = dat_links[dat_links.cmso_link_id .== tID, :cmso_object_id]
            fs = filter(x -> x.cmso_object_id in os, dat_objects)[:, :cmso_frame_id]
            objects = Dict{UInt32, UInt32}(zip(fs, os))

            next = Array{UInt32, 1}()
            next_event = nothing
            prev = Array{UInt32, 1}()
            prev_event = nothing

            start_tp = min(fs...)
            end_tp = max(fs...)

            at = AutoTracklet(tID, objects, next, next_event, prev, prev_event, start_tp, end_tp)
            push!(autotracklets, at)
        end
    end

    return autotracklets
end

function readImages(input::CSVDataFormat)

    images = Array{Array{RGBX{Normed{UInt8,8}},2},1}()

    if isnothing(input.images)
        dat = File(input.file) |> DataFrame!
        pad = UInt32(10)
        y = round(UInt32, max(dat[:, input.coord_col[1]]...)) + pad
        x = round(UInt32, max(dat[:, input.coord_col[2]]...)) + pad
        t = max(dat[:, input.frame_col]...)

        @showprogress for _ in 1:(t + 1)
            push!(images, zeros(RGBX{Normed{UInt8, 8}}, x, y))
        end
    else
        image_files = joinpath.(input.images, sort(readdir(input.images, false)))

        @showprogress for image_file in image_files
            push!(images, load(image_file))
        end
    end
    return images
end

function readImages(input::BioTracksDataFormat)
    @warn "readImages unimplemented for BioTracksDataFormat"

    images = Array{Array{RGBX{Normed{UInt8,8}},2},1}()
    open(input.meta, "r") do f
        j = JSON.parse(read(f, String))
        base = dirname(input.meta)
        file = [x["path"] for x in j["resources"] if x["name"] == "cmso_links_table"]
        (length(file) == 1 || @error "Either no or multiple resources called cmso_links_table") &&
            (file = joinpath(base, file[1]))

        dat_links = File(file) |> DataFrame!

        file = [x["path"] for x in j["resources"] if x["name"] == "cmso_objects_table"]
        (length(file) == 1 || @error "Either no or multiple resources called cmso_objects_table") &&
            (file = joinpath(base, file[1]))

        dat_objects = File(file) |> DataFrame!

        pad = UInt32(10)
        y = round(UInt32, max(dat_objects.cmso_x_coord...)) + pad
        x = round(UInt32, max(dat_objects.cmso_y_coord...)) + pad
        t = round(UInt32, max(dat_objects.cmso_frame_id...))

        @showprogress for _ in 1:(t + 1)
            push!(images, zeros(RGBX{Normed{UInt8, 8}}, x, y))
        end
    end

    return images
end

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table s begin
        "celltracker"
            help = "convert from the old CellTracker format"
            action = :command
        "cellprofiler"
            help = "convert from an exported csv file generated by CellProfiler"
            action = :command
        "ilastik"
            help = "convert from a *_Tracking-Result.h5 file generated by iLastik"
            action = :command
        "biotracks"
            help = "convert from the biotracks data format"
            action = :command
        "csv"
            help = "convert from a CSV file"
            action = :command
    end

    @add_arg_table s["celltracker"] begin
        "--tracks-xml", "-t"
            help = "the tracks.xml file"
            metavar = "FILE"
            required = true
        "--xml", "-x"
            help = "the xml folder"
            metavar = "DIR"
            required = true
        "--images", "--img", "-i"
            help = "the images folder"
            metavar = "DIR"
            required = true
        "--output", "-o"
            help = "the output file for the converted TraCurate HDF5 file"
            metavar = "FILE"
            required = true
    end

    @add_arg_table s["cellprofiler"] begin
        "--csv", "-c"
            help = "the csv file"
            metavar = "FILE"
            required = true
        "--output", "-o"
            help = "the output file for the converted TraCurate HDF5 file"
            metavar = "FILE"
            required = true
    end

    @add_arg_table s["ilastik"] begin
        "--hdf"
            help = "the hdf5 file"
            metavar = "FILE"
            required = true
        "--csv"
            help = "a CSV file exported by iLastik that contains the tracks"
            metavar = "FILE"
            required = false
        "--output", "-o"
            help = "the output file for the converted TraCurate HDF5 file"
            metavar = "FILE"
            required = true
        "--images", "--img", "-i"
            help = "the images folder"
            metavar = "DIR"
            required = false
    end

    @add_arg_table s["biotracks"] begin
        "--meta", "-m"
            help = "the JSON file containing all the metadata"
            metavar = "FILE"
            required = true
        "--output", "-o"
            help = "the output file for the converted TraCurate HDF5 file"
            metavar = "FILE"
            required = true
    end

    @add_arg_table s["csv"] begin
        "--file"
            help = "the CSV file to process"
            metavar = "FILE"
            required = true
        "--id-col"
            help = "the column that contains the object ID"
            metavar = "STRING"
            required = false
        "--frame-col"
            help = "the column that contains the frame ID"
            metavar = "STRING"
            required = true
        "--slice-col"
            help = "the column that contains the slice ID"
            metavar = "STRING"
            required = false
        "--chan-col"
            help = "the column that contains the channel ID"
            metavar = "STRING"
            required = false
        "--coord-cols"
            help = "a string of the form 'x_col_name:y_col_name' that holds the name of the columns that contain the x- and y-coordinates, separated by colon"
            metavar = "STRING:STRING"
            required = true
        "--bb-cols"
            help = "a string of the form 'tl_x_col:tl_y_col:br_x_col:br_y_col' that holds the name of the columns that contain the x- and y-coordinatesof the top left and bottom right points of the bounding box, separated by colon"
            metavar = "STRING:STRING:STRING:STRING"
            required = false
        "--track-col"
            help = "the column that contains the track ID"
            metavar = "STRING"
            required = false
        "--images", "-i"
            help = "the directory containing the images"
            metavar = "DIR"
            required = false
        "--flip-y"
            help = "flip the y axis"
            action = :store_true
        "--output", "-o"
            help = "the output file for the converted TraCurate HDF5 file"
            metavar = "FILE"
            required = true
    end

    return parse_args(s)
end

function eval_args(args::Dict{String, Any})
    cmd = args["%COMMAND%"]
    args = args[cmd]
    if cmd == "celltracker"
        out = TraCurateDataFormat(args["output"])
        in = CellTrackerDataFormat(args["tracks-xml"], args["xml"], args["images"])
    elseif cmd == "cellprofiler"
        out = TraCurateDataFormat(args["output"])
        in = CellProfilerDataFormat(args["csv"])
    elseif cmd == "ilastik"
        out = TraCurateDataFormat(args["output"])
        in = ILastikDataFormat(
            args["hdf"],
            args["csv"],
            "/exported_data",
            args["images"])
    elseif cmd == "biotracks"
        out = TraCurateDataFormat(args["output"])
        in = BioTracksDataFormat(args["meta"])
    elseif cmd == "csv"
        out = TraCurateDataFormat(args["output"])
        quirks = Dict{String,Bool}(
            "flip-y" => args["flip-y"])
        in = CSVDataFormat(
            args["file"],
            args["images"],
            if isnothing(args["id-col"]) nothing else Symbol(args["id-col"]) end,
            Symbol(args["frame-col"]),
            if isnothing(args["slice-col"]) nothing else Symbol(args["slice-col"]) end,
            if isnothing(args["chan-col"]) nothing else Symbol(args["chan-col"]) end,
            (Symbol.(split(args["coord-cols"], ":"))...,),
            if isnothing(args["bb-cols"]) nothing else (Symbol.(split(args["bb-cols"], ":"))...,) end,
            if isnothing(args["track-col"]) nothing else Symbol(args["track-col"]) end,
        quirks)
    end
    return convert(in, out)
end

if !isinteractive()
    return eval_args(parse_commandline())
end
