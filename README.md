### Installation

Just clone the repository somewhere, e.g. using

```git clone https://gitlab.com/tracurate/tcConvert```

To run ```tcConvert``` you will need to have a ```julia``` binary in your path.

For installing Julia, please follow the instructions provided on
https://julialang.org/

The Julia version should at least be ```1.3```.

To add Julia to your path, you can add something like

```export PATH="$PATH:/path/to/julia"```

to your ```.bashrc```, ```.zshrc``` or whatever shell you are using.

When running the script, on the first run, all dependencies will be downloaded
and installed in the current users Julia directory.

### Usage

```tcConvert``` provides a description of its arguments by running

```./tcConvert.jl -h```

For subcommands, the help can be accessed using

```./tcConvert.jl <subcommand> -h```
